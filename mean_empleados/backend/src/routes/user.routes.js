const { Router }= require('express');
const router=Router();
const user=require('../controllers/user.controller.js');


router.post('/', user.login);
router.post('/register', user.register);
module.exports=router;
