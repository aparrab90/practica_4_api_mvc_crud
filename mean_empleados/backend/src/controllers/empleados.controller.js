const empleadoCtrl = {};
const Empleado = require('../models/Empleado');

empleadoCtrl.getEmpleados = async (req, res) => {
    try {
        const empleados = await Empleado.find();
        res.status(200).json(empleados);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Error al obtener empleados' });
    }
}

empleadoCtrl.createEmpleado = async (req, res) => {
    try {
        const { nombre, cargo, departamento, sueldo } = req.body;

        if (!nombre || !cargo || !departamento || !sueldo) {
            return res.status(400).json({ message: 'Todos los campos son requeridos' });
        }

        const empleado = new Empleado({
            nombre,
            cargo,
            departamento,
            sueldo
        });

        await empleado.save();
        res.status(201).json({ message: 'Empleado creado exitosamente', empleado });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Error al crear empleado' });
    }
}

empleadoCtrl.editEmpleado = async (req, res) => {
    try {
        const _id = req.params.id;
        const { nombre, cargo, departamento, sueldo } = req.body;

        if (!_id) {
            return res.status(400).json({ message: 'ID del empleado es requerido' });
        }

        const empleado = {
            nombre,
            cargo,
            departamento,
            sueldo
        };

        const empleadoActualizado = await Empleado.findByIdAndUpdate(_id, { $set: empleado }, { new: true });

        if (!empleadoActualizado) {
            return res.status(404).json({ message: 'Empleado no encontrado' });
        }

        res.status(200).json({ message: 'Empleado actualizado exitosamente', empleadoActualizado });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Error al actualizar empleado' });
    }
}

empleadoCtrl.deleteEmpleado = async (req, res) => {
    try {
        const _id = req.params.id;

        if (!_id) {
            return res.status(400).json({ message: 'ID del empleado es requerido' });
        }

        const empleadoEliminado = await Empleado.findByIdAndRemove(_id);

        if (!empleadoEliminado) {
            return res.status(404).json({ message: 'Empleado no encontrado' });
        }

        res.status(200).json({ message: 'Empleado eliminado exitosamente' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Error al eliminar empleado' });
    }
}

module.exports = empleadoCtrl;