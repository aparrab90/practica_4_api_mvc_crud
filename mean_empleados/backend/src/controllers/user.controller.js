const userCtrl = {};
const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { SECRET_KEY } = require('../config');

userCtrl.login = async (req, res) => {
    const { username, password } = req.body;
    const user = await User.findOne({ username });
    
    if (!user) return res.status(401).send('Usuario no encontrado');
    const isValidPassword = await bcrypt.compare(password, user.password);
    if (!isValidPassword) return res.status(401).send('Contraseña incorrecta');
    const token = jwt.sign({ userId: user._id }, SECRET_KEY, { expiresIn: '1h' });

    res.json({ message: 'Login exitoso', token });
}

userCtrl.register = async (req, res) => {
    try {
        const { username, password } = req.body;
        const user = new User({ username, password });
        await user.save();
        res.send('Usuario registrado exitosamente');
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al registrar el usuario');
    }
}







module.exports = userCtrl;