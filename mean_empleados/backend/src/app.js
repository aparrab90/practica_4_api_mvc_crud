const express=require('express');
const morgan=require('morgan');
const cors=require('cors');
const app=express();

//settings
app.set('puerto',process.env.PORT|| 3000);
app.set('nombreApp','Gestión de empleados');
app.use(morgan('dev'));
app.use(express.json())
app.use(cors())
app.use('/api/empleados',require('./routes/empleados.routes'));
app.use('/login/empleados',require('./routes/user.routes'));

module.exports=app;
