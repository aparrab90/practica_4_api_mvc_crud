const {Schema, model}=require('mongoose');
const bcrypt = require('bcrypt');

const UserSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    }
});

UserSchema.pre('save', function (next) {
    const user = this;
    bcrypt.hash(user.password, 10, function (error, hash) {
        if (error) return next(error);
        user.password = hash;
        next();
    });
});

module.exports = model('User', UserSchema);
